package com.solvedo.training.demoproject.service;

import javax.jws.WebService;

import com.solvedo.training.demoproject.model.Country;

@WebService
public interface CountryService {
	
	Country readCountry(String countryName);
	void saveCountry(Country country);
	

}
