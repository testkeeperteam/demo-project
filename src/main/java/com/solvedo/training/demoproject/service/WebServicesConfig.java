package com.solvedo.training.demoproject.service;



import javax.xml.ws.Endpoint;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebServicesConfig {

    @Autowired
    private Bus bus;
    
    @Autowired
    private CountryServiceImpl serviceImpl;

    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, serviceImpl);
        endpoint.publish("/countries");
        return endpoint;
    }
}
