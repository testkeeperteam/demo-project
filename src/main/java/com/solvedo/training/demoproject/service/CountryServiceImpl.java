package com.solvedo.training.demoproject.service;

import javax.jws.WebParam;
import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import com.solvedo.training.demoproject.model.Country;
import com.solvedo.training.demoproject.repository.CountryRepository;


@Configuration
@WebService(endpointInterface="com.solvedo.training.demoproject.service.CountryService")
public class CountryServiceImpl implements CountryService{

	@Autowired
	private CountryRepository repository;
	
	
	@Override
	public Country readCountry(@WebParam(name = "countryName", targetNamespace = "") String countryName) {
		return repository.findCountry(countryName);
	}

	@Override
	public void saveCountry(@WebParam(name = "country", targetNamespace = "") Country country) {
		// TODO Auto-generated method stub
		
	}

}
